<?php 

class Cricket{

	function __construct(){
		echo "I am comes form Cricket class.<br/>";
	}
	
}

class Movie extends Cricket
{
	
	function __construct()
	{
		parent::__construct();
		echo "I am comes form Subclass of Cricket";
	}
}

$obj = new Movie;

$obj->__construct();