<?php
echo "<h2>____________ PHP Array Function_____________</h2><hr>";

$global_array = array('A','B','C','D','E','F','G');
echo "<h3>This is orginal array.</h3><pre/>";
print_r($global_array );

//array_chunk()
echo "<h3>** array_chunk() ** Split an array into chunks.</h3>";
		echo "<pre/>";
		print_r(array_chunk($global_array,3));
		echo "<h4>Output with  array_chunk() true parameter.</h4>";		
		print_r(array_chunk($global_array,3,true));

//array_combine()
echo "<h3>** array_combine() ** Creates an array by using one array for keys and another for its values.</h3>";	
		$color = array('color','green','red','yellow');
		$fruit = array('fruit name','avocado','apple','banana');
		$ma_color_name = array_combine($color,$fruit);
		echo "<pre/>";
		print_r($ma_color_name);

//array_fill()
echo "<h3>** array_fill() ** Fill an array with values.</h3>";
		$arr_fill1 = array_fill(3,5,'Hello friends');	
		$arr_fill2 = array_fill(-3,4,'Hello Bangladesh');
		echo "<pre/>";
		print_r($arr_fill1);
		print_r($arr_fill2);

//array_flip()
echo "<h3>** array_flip() **  Exchanges all keys with their associated values in an array.</h3>";	
		$arr_fliped = array_flip($fruit);
		echo "<pre/>";
		print_r($arr_fliped);	

//array_count_values()
echo "<h3>** array_count_values() **   Counts all the values of an array.</h3>";
	$arr_count = array('Apple',5,5,5,5,5,5,5,'Apple','avocado','apple','banana');
	echo "<pre/>";
	print_r(array_count_values($arr_count));

//array_key_exists()
echo "<h3>** array_key_exists() ** Checks if the given key or index exists in the array.</h3>";	
		$exists_key = array('One'=>1,'Two'=>2,'Three'=>3);
		echo "The output is array key exist for (One) value = ".array_key_exists('One',$exists_key);

//array_keys()
echo "<h3>** array_keys() ** Return all the keys or a subset of the keys of an array.</h3>";
		$arr_keys = array("blue", "red", "green", "blue", "blue");
		echo "<pre/>";
		print_r(array_keys($arr_keys));			
		$arr_keys = array("key1"=>'Sky', "key2"=>'Sun', "key3"=>'Grass');
		echo "<pre/>";
		print_r(array_keys($arr_keys,"Sun"));	

//array_merge()
echo "<h3>** array_merge() **  Merge one or more arrays.</h3>";	
			$arr_1 = array("color"=>'blue','Black','White');
			$arr_2 = array('Sky',7,6,"color"=>'red from second array');
			echo "<pre/>";
			print_r(array_merge($arr_1,$arr_2));

//array_pad()
echo "<h3>** array_pad() ** Pad array to the specified length with a value.</h3>";
		$arr_pad = array('blue','Black','White');
		echo "<pre/>";
		print_r(array_pad($arr_pad,10,'Sorry! this is array pad'));

//array_pop()
echo "<h3>** array_pop() ** Pop the element off the end of array.</h3>";
		$arr_pop = array("Orange","Jackfruit","Apple","cut by array pop Last value",);
		echo "<pre/>";
		$after_arr_pop = array_pop($arr_pop);
		echo ($after_arr_pop );
		print_r($arr_pop);
//array_push()
echo "<h3>** array_push() ** Push one or more elements onto the end of array.</h3>";
		$arr_push = array("Orange","Jackfruit","Apple");
				array_push($arr_push,"Olive new add","Papay new add");
		echo "<pre/>";
		print_r($arr_push);


//array_rand()
echo "<h3>** array_rand() ** Pick one or more random entries out of an array.</h3>";
		$arr_rand = array("Nothing", "Something", "HAthing");
		$rand_keys = array_rand($arr_rand,2);
		echo $arr_rand[$rand_keys[0]];


//array_reverse()
echo "<h3>** array_reverse() ** Return an array with elements in reverse order.</h3>";
		$arr_rev = array("Orange i am first","Jackfruit","Apple","Dalim i am last");
		$revers = array_reverse($arr_rev);
		echo "<pre/>";
		print_r($revers);
		echo "<h4>Out put with true parameter</h4>";		
		$arr_rev = array("Orange i am first","Jackfruit","Apple","Dalim i am last",);
		$revers = array_reverse($arr_rev,true);
		echo "<pre/>";
		print_r($revers);

//array_replace()
echo "<h3>** array_replace() ** Replaces elements from passed arrays into the first array.</h3>";
		$arr_1 = array("Orange","Jackfruit","Apple","Dalim");
		$arr_2 = array(0 => 'Red orange',1 => 'Blue Fruits');
		$arr_3 = array(0 => 'Not orange');
		$rep_array = array_replace($arr_1,$arr_2,$arr_3 );
		echo "<pre/>";
		print_r($rep_array);


//array_search()
echo "<h3>** array_search() **  Searches the array for a given value and returns the first corresponding key if successful.</h3>";
		$arr_search = array(0 => 'BD',1 => 'PAK',2 => 'PAK',3=>'IND');
		$key = array_search('PAK',$arr_search);
		echo "The search index number or name = ".$key;


//array_shift()
echo "<h3>** array_shift() ** Shift an element off the beginning of array .</h3>";
		$arr_shist = array("1st val Orange","2nd val Jackfruit","3rd val Apple","Dalim");
		array_shift($arr_shist);
		echo "<pre/>";
		print_r($arr_shist);

//array_unshift()
echo "<h3>** array_unshift() ** Prepend one or more elements to the beginning of an array .</h3>";
		$arr_unshist = array("1st val Orange","2nd val Jackfruit","3rd val Apple","Dalim");
		array_unshift($arr_unshist,"Add extra val","add new");
		echo "<pre/>";
		print_r($arr_unshist);


//array_unique()
echo "<h3>** array_unique() ** Removes duplicate values from an array .</h3>";
		$arr_uniq = array("a te"=>"Apple","b te"=>"Apple","c te"=>"Cate","d"=>"Cate");
		echo "<pre/>";
		print_r(array_unique($arr_uniq));



//array_values()
echo "<h3>** array_values() **  Return all the values of an array .</h3>";
		$arr_values = array("ab"=>"Here key are hide","a te"=>"Apple","b te"=>"Apple","c te"=>"Cate","d"=>"Cate");
		echo "<pre/>";
		print_r(array_values($arr_values));

//arsort()
echo "<h3>** arsort() **  Sort an array in reverse order and maintain index association .</h3>";
	$arr_sort = array('a'=>'Orange','f'=>'Apple','c'=>'Banana','b'=>'Green Apple');
	arsort($arr_sort);
	echo "<pre/>";
	print_r($arr_sort);


//asort()
echo "<h3>** asort() **  Sort an array and maintain index association .</h3>";

	$arr_sort = array('a'=>'Orange','f'=>'Apple','c'=>'Banana','b'=>'Green Apple');
	asort($arr_sort);
	echo "<pre/>";
	print_r($arr_sort);
//compact()
echo "<h3>** compact() **  Create array containing variables and their values .</h3>";

	$city = "Dhaka";
	$state = "2-DOSH";
	$money = "Taka";
	$arr_info = array("city","state");
	$arr_compact = compact("money",$arr_info);
	echo "<pre/>";
	print_r($arr_compact);

//in_array()
echo "<h3>** in_array() **  Checks if a value exists in an array .</h3>";

	$in_arr = array('a'=>'Orange','f'=>'Apple','c'=>'Banana','b'=>'Green Apple');
	if(in_array('Orange',$in_arr)){
		echo "The value found in array<br/>";
	}else{
		echo "The value not found in array<br/>";
	}

//shuffle()
echo "<h3>** shuffle() **  Shuffle an array .</h3>";
	$numbers = range(1,30);
	shuffle($numbers);
	echo "<pre/>";
	print_r($numbers);










for ($i=0; $i <4 ; $i++) { 
	echo "<br/>";
}
