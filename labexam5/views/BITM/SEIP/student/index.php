<?php 
	include_once('../../../vendor/autoload.php');

	use App\BITM\SEIP\Labexam;
	$obstd = new Labexam;
	$offsetData ="";

	if (isset($_POST['limit'])) {
		
			$noOfItemPerPage = $_POST['limit'];
		}else{
			$noOfItemPerPage =5;
	}

	if (isset($_GET['pageNo'])) {
			$noOfPage        = $_GET['pageNo']-1;
		}else{
			$noOfPage        = 0;
		}

	$offsetData = $noOfItemPerPage * $noOfPage;
	if ($offsetData == 0) {
		$offsetData = NULL;
	}

	$arr        = $obstd->index($noOfItemPerPage ,$offsetData);
	$numberOfRow =$arr['numOfRow'];

	if ($numberOfRow > $noOfItemPerPage) {
		$numberOfPage    = ceil($numberOfRow / $noOfItemPerPage);
	}else{
		$numberOfPage =NULL;
	}
	array_pop($arr);


?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
		.pagination {
		  margin: 0px auto;
		  text-align: center;
		  padding: 5px;
		}
		.pagination > a, button {
		  color: #205b10;
		  font-size: 20px;
		  font-weight: bold;
		  text-decoration: none;
		}
	</style>
</head>
<body>
<table>
	<tr>
		<th colspan="4"><h3>Dowanload as <a href="pdf.php?pageNo=<?php echo $noOfPage+1;?>">PDF</a></h3></th>

	</tr>	
	<tr>
		<th colspan="3"><h3>Name and Hobies List</h3></th>
		<th colspan="1">
			<form action="index.php" method="POST">
			<select name="limit">
				<option>5</option>
				<option>10</option>
				<option>15</option>
				<option>25</option>
			</select>
			<input type="submit" value="Set Limit" name="submit">
			</form>
		</th>
	</tr>
	<tr>
		<td colspan="5">

			<!-- Search Option -->
			<form action="search.php" method="GET">
				<input type="search" name="keyword">
				<input type="submit" value="Search" name="submit">
			</form>


		</td>
	</tr>
	<tr>
		<td colspan="2"><a href="create.php">Add Student</a></td>
		<td colspan="2"><a href="deleteList.php">Recycle Bin</a></td>
	</tr>
	<?php
		if (isset($_SESSION['msg'])) { ?>
		<tr> <td colspan="3">
		<?php 	echo $_SESSION['msg'];
			unset($_SESSION['msg']); ?>
		</td> </tr>
		<?php } 	?>
	<tr>
		<th>ID</th>
		<th>Title</th>
		<th>Hobies</th>
		<th>Action</th>
	</tr>

<?php 


	$serial = 1;
	$serial +=$offsetData;
	foreach ($arr as $key => $value) { ?>
			

	<tr>
		<td><?php echo $serial++;?></td>
		<td><?php echo  $value['title'];?></td>
		<td><?php
		
				$hobies = str_split($value['hobies']);
				$hobies = array_flip($hobies);
				$arlength = count($hobies);
	

				$i = 1;
				if(is_array($hobies)){
				foreach($hobies as $key=>$val){
					if($key == 1){
						echo "Gaming ";
					}elseif($key == 2){
						echo "Fishing ";
					}elseif($key == 3){
						echo "Reading ";
					}elseif($key == 4){
						echo "Sleeping ";
					}elseif($key == 5){
						echo "Drawing ";
					}elseif($key == 6){
						echo "Working ";
					}elseif($key == 7){
						echo "Singing ";
					}

					if ($i<$arlength) {
						echo ",";
					}
					$i++;

				}
			}

		?></td>
		<td>
			<a href="show.php?id=<?php echo  $value['id'];?>">View Details</a>||
			<a href="delete.php?id=<?php echo  $value['id'];  ?> ">Delete</a>
		</td>
	</tr>

<?php	}?>

</table>
 <section class="pagination">
 	<?php 
		if (isset($numberOfPage) AND !empty($numberOfPage)) {
			for($i=1; $i<=$numberOfPage; $i++){ ?>
		 	<a href="index.php?pageNo=<?php echo $i; ?>"> <button><?php echo $i." "; ?></button> </a>
		
	<?php } }?>

 </section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>
</html>