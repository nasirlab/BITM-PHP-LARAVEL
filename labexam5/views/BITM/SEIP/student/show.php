<?php 
	include_once('../../../vendor/autoload.php');

	use App\BITM\SEIP\Labexam;
	$obstd = new Labexam;

$arr = $obstd->setData($_GET);

$value = $obstd->show();

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
		.pagination {
		  margin: 0px auto;
		  text-align: center;
		  padding: 5px;
		}
		.pagination > a, button {
		  color: #205b10;
		  font-size: 20px;
		  font-weight: bold;
		  text-decoration: none;
		}
	</style>
</head>
<body>
<table>	
	<tr>
		<th colspan="2"><h3>Details</h3></th>
		<th colspan="2"><a href="index.php">Back to list</a></th>
		</tr>
	<tr>
		<th>ID</th>
		<th>Title</th>
		<th>Hobies</th>
		<th>Action</th>
	</tr>
	
	<tr>
		<td><?php echo  $value['id'];?></td>
		<td><?php echo  $value['title'];?></td>
		<td><?php
		
				$hobies = str_split($value['hobies']);
				$hobies = array_flip($hobies);
				$arlength = count($hobies);
	

				$i = 1;
				if(is_array($hobies)){
				foreach($hobies as $key=>$val){
					if($key == 1){
						echo "Gaming ";
					}elseif($key == 2){
						echo "Fishing ";
					}elseif($key == 3){
						echo "Reading ";
					}elseif($key == 4){
						echo "Sleeping ";
					}elseif($key == 5){
						echo "Drawing ";
					}elseif($key == 6){
						echo "Working ";
					}elseif($key == 7){
						echo "Singing ";
					}

					if ($i<$arlength) {
						echo ",";
					}
					$i++;

				}
			}

		?></td>
		<td>
			<a href="edit.php?id=<?php echo  $value['id'];?>">Edit</a>||
			<a href="delete.php?id=<?php echo  $value['id'];  ?> ">Delete</a>
		</td>
	</tr>
</table>

</body>
</html>