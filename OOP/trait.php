<?php 
/*
**Trait normally we are use for softdelete operetion
*
**Its work overwite by another function same name
*
**A Trait is similar to a class, but only intended to group functionality in a fine-grained and consistent way. It is not possible to instantiate a Trait on its own. It is an addition to traditional inheritance and enables horizontal composition of behavior; that is, the application of class members without requiring inheritance. 
*
**/


class Action {
	public function deleteData(){
		echo "Your data deleted parmanently<br/>";
	}
}

trait softdelete {
	public function deleteData(){
		parent::deleteData();
		echo "Your data deleted form application but contain as database<br/>";
		}
}
class Result extends Action {
	use softdelete;

}


$obj1 = new Result();

$obj1->deleteData();
