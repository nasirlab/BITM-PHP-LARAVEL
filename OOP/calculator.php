<?php 

class Calculator{

	public $num_1 = 10;
	public $num_2 = 20;

	public function getResult(){
		$result = $this->num_1 + $this->num_2;

		return $result;
	}

	public function setData($a){
		$this->num_1= $a;
	}
}


$obj = new Calculator;

$obj->setData(50);
$result = $obj->getResult();
echo "Output is= ".$result;