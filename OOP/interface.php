<?php 
/**
* Interface is empty class
* Only method define into this 
* method haven't boady part
* work by implementing to another class
* And possible to multiple interface implement in a class.
* directly object create are not posible for abstract class
**/



interface Subject{
	public function mySubject();
}

interface Fruit{
	public function myFruit();
}

interface Flower{
	public function myFlower();
}


class Student implements Subject,Fruit,Flower{
	public function __construct(){
		echo "As a student <br/>";
		$this->mySubject();
		$this->myFruit();
		$this->myFlower();
	}
	public function mySubject(){
		echo "My favourite subject is Math .<br/>";
	}	
	public function myFruit(){
		echo "My favourite fruit is Mango .<br/>";
	}	
	public function myFlower(){
		echo "My favourite flower is Whiterose .<br/>";
	}
}

new Student;



class Teacher implements Subject,Fruit,Flower{
	public function __construct(){
		echo "<br/>As a Teacher <br/>";
		$this->mySubject();
		$this->myFruit();
		$this->myFlower();
	}
	public function mySubject(){
		echo "My favourite subject is Physics .<br/>";
	}	
	public function myFruit(){
		echo "My favourite fruit is Orange .<br/>";
	}	
	public function myFlower(){
		echo "My favourite flower is Rose .<br/>";
	}
}

new Teacher;