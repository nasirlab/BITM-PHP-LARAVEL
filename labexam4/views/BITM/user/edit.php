<?php
include_once ('../../../vendor/autoload.php');

use App\BITM\user\User;
$objUser = new User;
$user =  $objUser->setData($_GET)->show();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit Data</title>
</head>
<body>
	<h1>Answer for Q-2<hr></h1>

	<fieldset>
		<legend>Add User information</legend>
		<form action="update.php" method="POST" >
			<table>
				<tr>
					<th colspan="3">
						<a href="index.php">User List</a>
					</th>
				</tr>
				<tr>
					<td>Eser name</td>
					<td><input type="text" value="<?php echo $user['user_name']; ?>" name="user_name"> </td>
				</tr>			
				<tr>
					<td>Email</td>
					<td><input type="email" value="<?php echo $user['email']; ?>"  name="email"></td>
				</tr>			
				<tr>
					<td>Pasword</td>
					<td><input type="pasword" value="<?php echo $user['pasword']; ?>"  name="pasword"> </td>
				</tr>			
				<tr>
					<td>Gender</td>
					<td><input type="radio" value="1" <?php if ($user['gender']==1) {echo 'checked=""';}?> name="gender""> Male</td>


					<td><input type="radio" value="2" <?php if ($user['gender']==2) {echo 'checked=""';}?> name="gender">Female</td>

				</tr>			
				<tr>

					<td>
						<input type="hidden" value="<?php echo $user['id'];?>" name="id">
						<input type="submit" value="Update" name="submit">
					</td>
					
				</tr>
			</table>
		</form>
	</fieldset>


</body>
</html>