<?php 


abstract class Person{

	public abstract function getName();
	protected abstract function getNumberOfPerson();

}


class Peoples extends Person{

	public function getName(){
		echo "Something peoples class<br/>";
	}
	public function getNumberOfPerson(){
		echo "Something getNumberOfPerson function<br/>";
	}	
}