<?php 
error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_DEPRECCATED);
error_reporting(E_ALL ^ E_DEPRECATED);
ini_set('display_errors',TRUE);
ini_set('display_startup_errors',TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI =='cli')
	die('This example should.......');

	include_once('../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
	include_once('../../../../vendor/autoload.php');
	use App\BITM\SEIP\student\Student;
	$obstd = new Student;
	$arr = $obstd->index();

$objPHPExcel= new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Test result file");


	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1','SL no')
	->setCellValue('B1','Student Name');

	$counter=2;
	$serial=0;


	foreach ($arr as $value) { 		
		$serial++;
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$counter, $serial)
		->setCellValue('B'.$counter, $value['title']);
		$counter++;
	}
		$objPHPExcel->getActiveSheet()->setTitle('Student_list');

		$objPHPExcel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.ms-excl');
		header('content-Disposition: attachment;filename="studentList.xls"');
		header('Cache-Control:max-age=0');
		header('Cache-Control:max-age=1');


		header('Expires:Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		header('Cache-Control: cache, must-revalidate');
		header('Pragma: public');


		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
