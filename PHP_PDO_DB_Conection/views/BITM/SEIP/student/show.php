<?php 
include_once('../../../../vendor/autoload.php');

use App\BITM\SEIP\student\Student;
$obstd = new Student;
$arr = $obstd->setData($_GET);

$value = $obstd->show();

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table{
			margin: 0px auto;
			background-color: #eef1ee;
			padding: 20px;
			min-width: 500px;
		}
		table td,table th{
			padding: 5px;
			border: 1px solid #000;
			text-align: center;
		}
	</style>
</head>
<body>
<table>
	<tr>
		<th colspan="3"><h3>Dowanload as <a href="pdf.php?id=<?php echo  $value['uniq_id'];?>">PDF</a></h3></th>
	</tr>
	<tr>
		<th colspan="3"><h3>Student Details</h3></th>
	</tr>
	<tr>
		<td colspan="3"><a href="index.php">Return list</a></td>
	</tr>
	<tr>
		<th>ID</th>
		<th>Title</th>
		<th>Action</th>
	</tr>
	<tr>
		<td><?php echo $value['id']; ?></td>
		<td><?php echo  $value['title'];?></td>
		<td>
			<a href="edit.php?id=<?php echo  $value['uniq_id'];?>">Edit</a> || 
			<a href="softdelete.php?id=<?php echo  $value['uniq_id'];?>">Delete</a>
		</td>
	</tr>


</table>


</body>
</html>