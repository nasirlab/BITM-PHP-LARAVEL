<?php 
include_once('../../../../vendor/autoload.php');

use App\BITM\SEIP\student\Student;
$obstd = new Student;
$value = $obstd->setData($_GET)->show();

		if (isset($_SESSION['msg'])) { 
			echo $_SESSION['msg'];
			unset($_SESSION['msg']); 
		}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Add Student information</title>
</head>
<body>
	<fieldset>
		<legend>Student Info.</legend>
		<a href="index.php">View Student</a>
		<form action="update.php" method="POST">

			<input type="text" value="<?php echo $value['title'];?> " name="title">
			
			<input type="hidden" value="<?php echo $value['uniq_id'];?> " name="id">
			<input type="submit" value="Update" name="submit">
		</form>
	</fieldset>
</body>
</html>